export class Options {

  async: true;
  crossDomain: true;
  contentType: "application/json";
  dataType: "json";

  method: string;
  url: string;
  data: Object;

  constructor(url: string, method: string, data?: Object) {
    this.url = url;
    this.method = method;
    this.data = data || {};
  }
}

