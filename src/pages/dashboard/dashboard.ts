import { Component } from '@angular/core';
import { NavController} from 'ionic-angular';

import {SubmitTicketPage } from "../submit-ticket/submit-ticket";
import { PublicUserAccountInfoProvider } from "../../providers/public-user/public-user";
import {OtherProjectSelectLaPage} from "../other-project-select-la/other-project-select-la";
import {ProfilePage} from "../profile/profile";

@Component({
  selector: 'page-home',
  templateUrl: 'dashboard.html'
})
export class DashBoardPage {

  constructor(public navCtrl: NavController,
              private pbUSer:PublicUserAccountInfoProvider) {

    this.pbUSer.getProfileData();

  }

  pushToSubmitTicketPage(){
    this.navCtrl.push(SubmitTicketPage);
  }

  openOtherProjectPage() {
    this.navCtrl.push(OtherProjectSelectLaPage);
  }

  openProfilePage() {
    this.navCtrl.push(ProfilePage);
  }
}
