import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController} from 'ionic-angular';
import { UserComplaintTicketProvider } from "../../providers/complaint-ticket/complaint-ticket";
import { UserComplaintTicket } from "../../../../fyp_everything_modules/model/complaint/UserComplaintTicket";

@IonicPage()
@Component({
  selector: 'page-ticket',
  templateUrl: 'ticket.html',
})
export class TicketPage {

  public myComplaintTicket = new UserComplaintTicket();

  constructor(public viewCtrl:ViewController,
              public navPara:NavParams,
              public userTicketProvider:UserComplaintTicketProvider) {

    this.myComplaintTicket = this.userTicketProvider.list[navPara.get('indexNo')];

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
