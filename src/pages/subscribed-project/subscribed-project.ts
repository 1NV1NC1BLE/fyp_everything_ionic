import {Component} from '@angular/core';
import {IonicPage, ModalController, NavController, NavParams} from 'ionic-angular';
import {SubscribedMaintenanceProjectProvider} from "../../providers/project/subscribed-project";
import {ProjectDetailsPage} from "../project-details/project-details";
import {ProjectProgressPage} from "../project-progress/project-progress";
import {ProjectFeedbackPage} from "../project-feedback/project-feedback";

/**
 * Generated class for the SubscribedProjectPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscribed-project',
  templateUrl: 'subscribed-project.html',
})
export class SubscribedProjectPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public subsProjectProvider: SubscribedMaintenanceProjectProvider) {
    this.refreshData();
  }

  pushToProjectDetails(index: number) {
    let myProjectDetail = this.subsProjectProvider.list[index];
    let modal = this.modalCtrl.create(ProjectDetailsPage, {
      projectDetail: myProjectDetail,

    });
    modal.present();

  }

  pushToProjectProgress(index: number) {
    let myProjectID = this.subsProjectProvider.list[index].id;
    let modal = this.modalCtrl.create(ProjectProgressPage, {
      projectID: myProjectID,

    });
    modal.present();
    /*this.navCtrl.push(ProjectProgressPage, {
      projectID: myProjectID,
     // isSubscribeProjectMode: true
    });*/
  }

  pushToProjectFeedback(index: number) {
    let myProjectID = this.subsProjectProvider.list[index].id;
    this.navCtrl.push(ProjectFeedbackPage, {projectID: myProjectID});
  }

  refreshData() {
    this.subsProjectProvider.fetchUserSubscribedProjectList();
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
