import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscribedProjectPage } from './subscribed-project';

@NgModule({
  declarations: [
    SubscribedProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscribedProjectPage),
  ],
})
export class SubscribedProjectPageModule {}
