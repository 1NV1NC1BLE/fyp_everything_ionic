import {Component} from '@angular/core';
import {IonicPage, NavController, AlertController, ToastController, LoadingController} from 'ionic-angular';
import {Validators, FormGroup, FormControl} from "@angular/forms";

import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {LoginPage} from "../login/login";


@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})

export class RegisterPage {
  private regGroup: FormGroup;

  constructor(public navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private toastCtrl: ToastController) {

    this.regGroup = new FormGroup({

      email: new FormControl('', {
        validators: [Validators.required, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')],
        asyncValidators: [this.isEmailExists],

      }),

      passwords: new FormGroup({
        password: new FormControl('', [Validators.required, Validators.minLength(6)]),
        password_confirmation: new FormControl('', Validators.required)
      }, this.checkPasswords),

      firstName: new FormControl('', [Validators.required, this.isName]),
      lastName: new FormControl('', [Validators.required, this.isName]),
      dob: new FormControl('', Validators.required),
      contact: new FormControl('', [Validators.required, this.isValidContactNo])

    });
  }

  onSubmitRegForm() {

    let submitRegistration = () => {
      return new Promise((resolve, reject) => {

        let registerData = JSON.stringify(this.regGroup.getRawValue());

        let mySettings = new Settings('register');
        let myAjax = new AjaxHandler(mySettings);

        myAjax.postMethod(registerData,

          (response) => {
            window.localStorage.setItem('token', response.token);
            resolve();

          }, (response) => {
            reject();
          }
        );
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting...'});
    loading.present();

    submitRegistration().then(() => {

      let alert = this.alertCtrl.create({
        title: 'Registration Successful!',
        message: 'An verification link has been sent to your email!<br>Please click the link to verify your email and complete the registration process',
        buttons: ['OK']
      });
      alert.present().then(() => {
        this.navCtrl.setRoot(LoginPage);
      });

    }, () => {
      let toast = this.toastCtrl.create({
        message: "Something Bad Happened! Please try again later!",
        duration: 3000,
        position: 'bottom'
      });

      toast.present();
    }).then(() => {
      loading.dismiss();
    });

  }

  isEmailExists(frmCtrl: FormControl) {

    return new Promise(function (resolve) {

          let mySettings = new Settings('register/check-email');

          let myAjax = new AjaxHandler(mySettings);

          let data = JSON.stringify({email: frmCtrl.value});

          myAjax.postMethod(data, function (response) {

            resolve(null);

          }, function (xhr, status, error) {

            resolve({exist: true});
          });

      });

  }

  checkPasswords(group: FormGroup) {
    let pass = group.controls.password.value;
    let confirmPass = group.controls.password_confirmation.value;

    return pass === confirmPass ? null : {mismatch: true}
  }

  isName(regFrmCtrl: FormControl): any {

    if (regFrmCtrl.value === "")
      return null;
    if (regFrmCtrl.dirty) {
      let regExp = /^[a-zA-Z ]+$/;

      if (!regExp.test(regFrmCtrl.value)) {

        return {"invalid": true};
      } else {

        return null;
      }
    }
  }

  isValidContactNo(regFrmCtrl: FormControl): any {
    if (regFrmCtrl.value === "")
      return null;

    if (regFrmCtrl.dirty) {
      let regExp = /^0(10|12|13|14|16|17|18|19)\d{7,8}$/;

      if (!regExp.test(regFrmCtrl.value)) {

        return {"invalid": true};
      }
      else {
        return null;
      }
    }
  }

}

