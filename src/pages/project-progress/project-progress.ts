import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController} from 'ionic-angular';
import {ProjectProgressProvider} from "../../providers/project-progress/project-progress";

@IonicPage()
@Component({
  selector: 'page-project-progress',
  templateUrl: 'project-progress.html',
})
export class ProjectProgressPage {

  projectID: number;
  isSubscribeMode:boolean;

  constructor(public navCtrl: NavController,
              public navParam: NavParams,
              public viewCtrl:ViewController,
              public progressProvider: ProjectProgressProvider) {

    this.projectID = this.navParam.get('projectID');
    //this.isSubscribeMode = this.navParam.get('isSubscribeProjectMode');

    this.refreshData();
  }

  refreshData() {

    this.progressProvider.fetchAllRecords(this.projectID);
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);

  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
