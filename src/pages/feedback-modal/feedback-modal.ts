import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ViewController, AlertController, LoadingController} from 'ionic-angular';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

@IonicPage()
@Component({
  selector: 'page-feedback-modal',
  templateUrl: 'feedback-modal.html',
})
export class FeedbackModalPage {

  public feedbackFormGroup: FormGroup;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private loadingCtrl: LoadingController,
              public viewCtrl: ViewController,
              public alertCtrl: AlertController) {

    this.feedbackFormGroup = new FormGroup({
      rating: new FormControl(this.navParams.get('rating'), [Validators.min(1), Validators.max(5), Validators.required]),
      title: new FormControl(this.navParams.get('title'), [Validators.required]),
      message: new FormControl(this.navParams.get('message'), [Validators.required]),
      projectID: new FormControl(this.navParams.get('projectID')),
      feedbackID: new FormControl(this.navParams.get('feedbackID'))
    }, this.checkRating)

  }

  checkRating(grp:FormGroup){
    let rate = grp.controls['rating'].value;
    if(rate == null || rate == 0)
      return {error:true};
    else
      return null;
  }

  onSubmitFeedbackForm() {

    let feedbackID = this.feedbackFormGroup.controls['feedbackID'].value;
    let data = JSON.stringify(this.feedbackFormGroup.getRawValue());

    let mySettings = new Settings('subscribed-project/feedback/submit');
    mySettings.token = window.localStorage.getItem('token');
    let myAjax = new AjaxHandler(mySettings);

    let submitNewFeedback = () => {
      return new Promise((resolve, reject) => {
        myAjax.postMethod(data, () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    let updateFeedback = () => {
      return new Promise((resolve, reject) => {
        myAjax.putMethod([feedbackID], data, () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    let feedbackMode = () => {
      return new Promise(resolve => {

        //if it is in insert mode
        if (feedbackID == null) {
          submitNewFeedback().then(() => {
            this.displayAlertBox('Feedback Submitted!', 'Thanks for your valuable feedback!<br>You may review your feedback again at any time!')

          }, () => {
            this.displayAlertBox('Opps, something bad happened!', 'Feedback not submitted!<br>Please try again later!');
          });

          //if it is in edit mode
        } else {
          updateFeedback().then(() => {

            this.displayAlertBox('Feedback Updated!', 'Thanks again for updating your feedback!<br>Together, lets make Malaysia great again!')
          }, () => {

            this.displayAlertBox('Opps, something bad happened!', 'Feedback not updated!<br>Please try again later!');
          });
        }
        resolve();
      });
    };

    let loading = this.loadingCtrl.create({content: 'Submitting...'});
    loading.present();
    feedbackMode().then(() => {
      loading.dismiss();
      this.dismiss();
    })

  }

  displayAlertBox(title: string, message: string) {
    let alert = this.alertCtrl.create({
      title: title,
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
