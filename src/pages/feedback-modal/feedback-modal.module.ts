import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FeedbackModalPage } from './feedback-modal';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    FeedbackModalPage,
  ],
  imports: [
    IonicPageModule.forChild(FeedbackModalPage),
    Ionic2RatingModule,
  ],
})
export class FeedbackModalPageModule {}
