import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TicketPage } from "../ticket/ticket";
import { MessagePage } from "../message/message";
import { UserComplaintTicketProvider } from "../../providers/complaint-ticket/complaint-ticket";

@IonicPage()
@Component({
  selector: 'page-view-ticket',
  templateUrl: 'view-ticket.html',
})
export class ViewTicketPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public ticketListProvider:UserComplaintTicketProvider) {
    this.reloadData();
  }

  presentDescriptionModal(index:number) {
    let modal = this.modalCtrl.create(TicketPage, {indexNo:index});
    modal.present();
  }

  presentMessageModal(index:number) {
    let modal = this.modalCtrl.create(MessagePage, {indexNo:index});
    modal.present();
  }

  reloadData(){
    this.ticketListProvider.fetchAllRecords();
  }

  doRefresh(refresher) {

    this.reloadData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }

}
