import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProjectFeedbackPage } from './project-feedback';
import { MatButtonModule, MatInputModule, MatStepperModule, MatFormFieldModule, MatIconModule, MatAutocompleteModule } from '@angular/material';
import { Ionic2RatingModule } from 'ionic2-rating';

@NgModule({
  declarations: [
    ProjectFeedbackPage,
    
  ],
  imports: [
    IonicPageModule.forChild(ProjectFeedbackPage),
    Ionic2RatingModule,
  ],
})
export class ProjectFeedbackPageModule {}
