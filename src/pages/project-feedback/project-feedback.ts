import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams, ModalController} from 'ionic-angular';
import {ProjectFeedbackProvider} from "../../providers/project-feedback/project-feedback";
import {FeedbackModalPage} from "../feedback-modal/feedback-modal";

@IonicPage()
@Component({
  selector: 'page-project-feedback',
  templateUrl: 'project-feedback.html',
})
export class ProjectFeedbackPage {

  private projectID;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public modalCtrl: ModalController,
              public feedbackProvider: ProjectFeedbackProvider){

    this.projectID = this.navParams.get('projectID');
    this.refreshData();

  }

  refreshData() {

    this.feedbackProvider.fetchAllRecords(this.projectID);

  }

  //pop feedback modal, depends on the editMode boolean value
  displayFeedbackModal(isEditMode: boolean) {

    if (!isEditMode) {
      let model = this.modalCtrl.create(FeedbackModalPage, {projectID: this.projectID});
        model.onDidDismiss(()=>{
          this.refreshData();
        });
      model.present();
    }
    else {

      let model = this.modalCtrl.create(FeedbackModalPage, {
        projectID: this.projectID,
        title: this.feedbackProvider.myFeedback.title,
        message: this.feedbackProvider.myFeedback.message,
        rating: this.feedbackProvider.myFeedback.rating,
        feedbackID: this.feedbackProvider.myFeedback.id,

      });
      model.onDidDismiss(()=>{
        this.refreshData();
      });
      model.present();
    }
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}

