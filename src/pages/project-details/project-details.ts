import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { MaintenanceProject } from "../../../../fyp_everything_modules/model/m_project/MaintenanceProject";

@IonicPage()
@Component({
  selector: 'page-project-details',
  templateUrl: 'project-details.html',
})
export class ProjectDetailsPage {

  public myProject = new MaintenanceProject();
  public isSubscribeMode:boolean;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public viewCtrl:ViewController) {

    this.myProject = this.navParams.get('projectDetail');
  }

  dismiss(){
    this.viewCtrl.dismiss();
  }

}
