import {Component} from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams, ToastController} from 'ionic-angular';
import {PublicUserAccountInfoProvider} from "../../providers/public-user/public-user";
import {ChangePasswordPage} from "../change-password/change-password";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public toastCtrl: ToastController,
              private loadingCtrl: LoadingController,
              public myUserInfo: PublicUserAccountInfoProvider) {

    this.myUserInfo.getProfileData();
  }

  pushChangePasswordPage() {
    this.navCtrl.push(ChangePasswordPage);
  }

  onClickChangeProfilePic() {
    document.getElementById('profilePic').click();

  }

  submitPic(id) {

    let uploadProfilePic = () => {
      return new Promise((resolve, reject) => {
        let formData = new FormData();
        formData.append('profilePic', (<HTMLInputElement>document.getElementById(id)).files[0]);

        let mySetting = new Settings('profile/public-user/change-avatar');
        mySetting.token = window.localStorage.getItem('token');
        mySetting.acceptHeader = undefined;
        mySetting.contentTypeHeader = undefined;
        mySetting.processData = false;
        mySetting.mimeType = "multipart/form-data";
        mySetting.contentType = false;

        let myAjax = new AjaxHandler(mySetting);
        myAjax.postMethod(formData, () => {

          resolve();
        }, (response) => {

          reject(response);
        });
      });
    };

    let loading = this.loadingCtrl.create({content: 'Processing...'});
    loading.present();

    uploadProfilePic().then(() => {

      this.presentToast("Profile picture changed!");
      this.myUserInfo.getProfileData();

    }, (response) => {

      if (response.status == 400)
        this.presentToast("No image is uploaded!");
      else
        this.presentToast("Opps, something bad happened!");

    }).then(() => {
      loading.dismiss();
    });
  }

  presentToast(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 3000,
      position: 'bottom'
    });

    toast.present();
  }

}
