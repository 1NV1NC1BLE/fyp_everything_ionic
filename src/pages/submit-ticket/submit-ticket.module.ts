import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubmitTicketPage } from './submit-ticket';
import { MatButtonModule, MatInputModule, MatStepperModule, MatFormFieldModule, MatIconModule, MatAutocompleteModule } from '@angular/material';

@NgModule({
  declarations: [
    SubmitTicketPage,
  ],
  imports: [
    IonicPageModule.forChild(SubmitTicketPage),
    MatButtonModule,
    MatInputModule,
    MatStepperModule,
    MatFormFieldModule,
    MatIconModule,
    MatAutocompleteModule,
  ],
})
export class SubmitTicketPageModule {}
