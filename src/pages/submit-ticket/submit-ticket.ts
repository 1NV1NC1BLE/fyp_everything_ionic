import {
  IonicPage,
  NavController,
  NavParams,
  ToastController,
  AlertController, LoadingController
} from 'ionic-angular';

import {Component} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Geolocation} from "@ionic-native/geolocation";

import {DashBoardPage} from "../dashboard/dashboard";
import {LocalAuthorityProvider} from "../../providers/local-authority/local-authority";
import {FacilityTypeProvider} from "../../providers/facility-type/facility-type";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

import * as $ from "jquery";

@IonicPage()
@Component({
  selector: 'page-submit-ticket',
  templateUrl: 'submit-ticket.html',
})

export class SubmitTicketPage {

  selectLAGroup: FormGroup;
  describeTicketGroup: FormGroup;
  getLocationGroup: FormGroup;
  uploadImageGroup: FormGroup;

  lastImage: string = null;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              private toastCtrl: ToastController,
              private alertCtrl: AlertController,
              private myGeo: Geolocation,
              public laProvider: LocalAuthorityProvider,
              public ftProvider: FacilityTypeProvider,
  ) {
    //Step 1: Select a Local Authority
    this.selectLAGroup = new FormGroup({
      localAuthority: new FormControl('', [Validators.required])
    });
    //Step 2: Describe Your Ticket
    this.describeTicketGroup = new FormGroup({
      title: new FormControl('', [Validators.required]),
      description: new FormControl('', [Validators.required]),
      facilityType: new FormControl('', [Validators.required]),
    });
    //Step 3: Provide Location
    this.getLocationGroup = new FormGroup({
      xCor: new FormControl(''),
      yCor: new FormControl(''),
      address: new FormControl('')
    }, this.locationGroupValidator);
    //Step 4: Upload Image
    this.uploadImageGroup = new FormGroup({
      firstImgID: new FormControl(''),
      secondImgID: new FormControl(''),
      thirdImgID: new FormControl(''),
    }, this.imageGroupValidator);

    laProvider.fetchAllRecords();
    ftProvider.fetchAllRecords();
  }

  imageGroupValidator(group: FormGroup) {
    let firstImg = group.controls.firstImgID.value;
    let secondImg = group.controls.secondImgID.value;
    let thirdImg = group.controls.thirdImgID.value;

    if (firstImg == '' && secondImg == '' && thirdImg == '') {
      return {zeroImg: true}
    }
    else
      return null;
  }

  locationGroupValidator(group: FormGroup) {

    let x = group.controls.xCor.value;
    let y = group.controls.yCor.value;
    let address = group.controls.address.value;

    if ((x == '' && y == '') && address == '') {
      return {empty: true};
    }
    return null
  }

  addressSelected() {
    this.getLocationGroup.controls['xCor'].setValue('');
    this.getLocationGroup.controls['yCor'].setValue('');
  }

  gpsCordinateSelected() {

    this.getLocationGroup.controls['address'].setValue('');

    this.myGeo.getCurrentPosition().then((resp) => {
      this.getLocationGroup.controls['xCor'].setValue(resp.coords.latitude);
      this.getLocationGroup.controls['yCor'].setValue(resp.coords.longitude);
      console.log(this.getLocationGroup.controls['xCor'].value);
      console.log(this.getLocationGroup.controls['yCor'].value);

    }).catch((error) => {
      console.log('Error getting location', error);
    });

  }

  setFileName(id) {

    switch (id) {
      case 'firstImgID':
        this.uploadImageGroup.controls[id].setValue((<HTMLInputElement>document.getElementById(id)).files[0].name);
        (<HTMLImageElement>document.getElementById('firstOutput')).src = window.URL.createObjectURL((<HTMLInputElement>document.getElementById(id)).files[0]);
        break;

      case 'secondImgID':
        this.uploadImageGroup.controls[id].setValue((<HTMLInputElement>document.getElementById(id)).files[0].name);
        (<HTMLImageElement>document.getElementById('secondOutput')).src = window.URL.createObjectURL((<HTMLInputElement>document.getElementById(id)).files[0]);
        break;

      case 'thirdImgID':
        this.uploadImageGroup.controls[id].setValue((<HTMLInputElement>document.getElementById(id)).files[0].name);
        (<HTMLImageElement>document.getElementById('thirdOutput')).src = window.URL.createObjectURL((<HTMLInputElement>document.getElementById(id)).files[0]);
        break;
    }
  }

  onClickSelectImgButton(img) {

    if (img == 'first') {
      document.getElementById('firstImgID').click();
    } else if (img == 'second') {
      document.getElementById('secondImgID').click();
    } else if (img == 'third') {
      document.getElementById('thirdImgID').click();
    }
  }

  onSubmitTicket() {
    let submitLoading = this.loadingCtrl.create({content: "Submitting..."});

    let submitTicket = () => {
      return new Promise((resolve, reject) => {

        let formData = new FormData();
        let data = {};
        data = $.extend(data, this.selectLAGroup.getRawValue(), this.describeTicketGroup.getRawValue(), this.getLocationGroup.getRawValue());
        formData.append("photo1", (<HTMLInputElement>document.getElementById('firstImgID')).files[0]);
        formData.append("photo2", (<HTMLInputElement>document.getElementById('secondImgID')).files[0]);
        formData.append("photo3", (<HTMLInputElement>document.getElementById('thirdImgID')).files[0]);
        formData.append('data', JSON.stringify(data));

        let mySetting = new Settings('complaint-ticket/submit');
        mySetting.token = window.localStorage.getItem('token');
        mySetting.acceptHeader = undefined;
        mySetting.contentTypeHeader = undefined;
        mySetting.processData = false;
        mySetting.mimeType = "multipart/form-data";
        mySetting.contentType = false;

        let myAjax = new AjaxHandler(mySetting);
        myAjax.postMethod(formData, () => {
          resolve();
        }, () => {
          reject();
        });
      });
    };

    submitLoading.present().then(() => {

      submitTicket().then(() => {
        submitLoading.dismiss();
        this.presentAlert("Ticket Submitted!", "You are now able to track your ticket status at View Ticket Page!");
        this.navCtrl.setRoot(DashBoardPage);

      }, () => {
        submitLoading.dismiss();
        this.presentAlert("Fail to submit ticket!", "Please try again later!");
        this.navCtrl.setRoot(DashBoardPage);
      })
    });

  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  presentAlert(title: string, subtitle: string) {
    let alert = this.alertCtrl.create({
      title: title,
      message: subtitle,
      buttons: ['OK']
    });
    alert.present();
  }
}
