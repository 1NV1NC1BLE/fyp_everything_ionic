import {Component} from '@angular/core';
import {IonicPage, NavParams, ViewController} from 'ionic-angular';
import {MessagesProvider} from "../../providers/messages/messages";
import {UserComplaintTicketProvider} from "../../providers/complaint-ticket/complaint-ticket";

/**
 * Generated class for the MessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-message',
  templateUrl: 'message.html',
})
export class MessagePage {

  constructor(private viewCtrl: ViewController,
              private navPar: NavParams,
              public  mesProvider: MessagesProvider,
              public ticketProvider: UserComplaintTicketProvider) {
    this.refreshData();
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  refreshData() {
    let ctID = this.ticketProvider.list[this.navPar.get('indexNo')];
    this.mesProvider.fetchAllRecords(ctID.id);
  }

  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }
}
