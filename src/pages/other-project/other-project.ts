import {Component} from '@angular/core';
import {AlertController, IonicPage, ModalController, NavController, NavParams, ToastController} from 'ionic-angular';
import {OtherProjectProvider} from "../../providers/other-project/other-project";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {ProjectDetailsPage} from "../project-details/project-details";

@IonicPage()
@Component({
  selector: 'page-other-project',
  templateUrl: 'other-project.html',
})
export class OtherProjectPage {

  laAcronym: string;
  laID: number;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public modalCtrl: ModalController,
              public otherProjectProvider: OtherProjectProvider) {

    this.laAcronym = this.navParams.get('acronym');
    this.laID = this.navParams.get('laID');

    this.refreshData();
  }

  refreshData() {
    this.otherProjectProvider.fetchLocalAuthorityProjectList(this.laID);
  }


  doRefresh(refresher) {

    this.refreshData();

    setTimeout(() => {
      refresher.complete();
    }, 3000);
  }


  promptConfirmSubscribeBox(index: number) {
    let alert = this.alertCtrl.create({
      title: 'Confirm to subscribe?',
      message: 'With great power comes great responsibility, ' +
      '<br> You are responsible to monitor and provide feedback to the project once you subscribed it.',

      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
          }
        },
        {
          text: 'Confirm',
          handler: () => {
            let projectID = this.otherProjectProvider.list[index].id;
            this.subscribeProject(projectID);
          }
        }
      ]
    });
    alert.present();
  }

  subscribeProject(projectID) {

    let mySetting = new Settings('other-project/subscribe');
    mySetting.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySetting);
    myAjax.getWithParaMethod([projectID],
      () => {
        this.presentToast("A project has been subscribed!");
        this.refreshData();

      }, () => {
        this.presentToast("Opps, something bad happened!");
      });

  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }

  pushToProjectDetails(index: number) {
    let myProjectDetail = this.otherProjectProvider.list[index];
    let modal = this.modalCtrl.create(ProjectDetailsPage, {
      projectDetail: myProjectDetail,
    });
    modal.present();

  }
}
