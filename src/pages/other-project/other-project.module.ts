import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherProjectPage } from './other-project';

@NgModule({
  declarations: [
    OtherProjectPage,
  ],
  imports: [
    IonicPageModule.forChild(OtherProjectPage),
  ],
})
export class OtherProjectPageModule {}
