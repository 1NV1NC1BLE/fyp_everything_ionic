import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import { FormControl, FormGroup, Validators} from "@angular/forms";
import {LoginPage} from "../login/login";

@IonicPage()
@Component({
  selector: 'page-forgot-pass',
  templateUrl: 'forgot-pass.html',
})
export class ForgotPassPage {

  private forgotPassFormGroup: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController) {

    this.forgotPassFormGroup = new FormGroup({
      email: new FormControl ('', [Validators.required, Validators.email])
      })

  }

  onSubmitResetPassForm(){
    let alert = this.alertCtrl.create({
      title: 'Password reset link sent!',
      message: 'An link has been sent to your email!<br>Please click the link to reset your password.',
      buttons: ['OK']
    });
    alert.present().then(() => {
      this.navCtrl.setRoot(LoginPage);
    });

  }


}
