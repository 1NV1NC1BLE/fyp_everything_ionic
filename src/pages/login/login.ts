import {Component} from '@angular/core';
import {IonicPage, NavController, NavParams} from 'ionic-angular';
import {Validators, FormBuilder, FormGroup} from "@angular/forms";
import {LoadingController} from "ionic-angular";
import {ToastController} from "ionic-angular";

import {DashBoardPage} from "../dashboard/dashboard";
import {RegisterPage} from "../register/register";
import {ForgotPassPage} from "../forgot-pass/forgot-pass"
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  private loginFormGroup: FormGroup;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public loadingCtrl: LoadingController,
              public formBuilder: FormBuilder,
              private toastCtrl: ToastController) {

    this.loginFormGroup = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      remember: ['false']
    });

  }

  onLogin() {

    let loading = this.loadingCtrl.create({content: "Logging In..."});

    let login = () => {
      return new Promise((resolve, reject) => {

        let mySettings = new Settings('login/public-user');
        let myAjax = new AjaxHandler(mySettings);
        myAjax.postMethod(JSON.stringify(this.loginFormGroup.getRawValue()),
          (response) => {
            window.localStorage.setItem('token', response.token);
            window.localStorage.setItem('isRemember', this.loginFormGroup.controls['remember'].value);
            this.navCtrl.setRoot(DashBoardPage);
            resolve();

          }, (response) => {

            let errMsg;
            let code = response.status;
            if (code == 422) {
              errMsg = "Invalid Email Address or Password!";
            }
            else {
              errMsg = "Opps! Something Bad Happened!";
            }
            this.presentToast(errMsg);
            reject(errMsg);
          });
      });
    };
    loading.present().then(() => {
      login().then(() => {
        loading.dismiss();
      }, ()=>{
        loading.dismiss();
      });
    });

  }

  pushForgetPasswordPage() {
    this.navCtrl.push(ForgotPassPage);
  }

  pushRegisterPage() {
    this.navCtrl.push(RegisterPage);
  }

  presentToast(msg: string) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
}
