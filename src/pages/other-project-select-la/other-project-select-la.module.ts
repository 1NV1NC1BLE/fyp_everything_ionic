import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OtherProjectSelectLaPage } from './other-project-select-la';

@NgModule({
  declarations: [
    OtherProjectSelectLaPage,
  ],
  imports: [
    IonicPageModule.forChild(OtherProjectSelectLaPage),
  ],
})
export class OtherProjectSelectLaPageModule {}
