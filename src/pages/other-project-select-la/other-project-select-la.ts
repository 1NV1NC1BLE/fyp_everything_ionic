import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { LocalAuthorityProvider } from "../../providers/local-authority/local-authority";
import { OtherProjectPage } from "../other-project/other-project";

/**
 * Generated class for the OtherProjectSelectLaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-other-project-select-la',
  templateUrl: 'other-project-select-la.html',
})
export class OtherProjectSelectLaPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public laProvider: LocalAuthorityProvider) {

    laProvider.fetchAllRecords();
  }

  pushToOtherProjectPage(myIndex:number){

    let myLaID = this.laProvider.list[myIndex].id;
    let myAcronym = this.laProvider.list[myIndex].acronym;

    this.navCtrl.push(OtherProjectPage, {
      acronym: myAcronym,
      laID:myLaID

    });
  }

}
