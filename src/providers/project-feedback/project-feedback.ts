import {Injectable} from '@angular/core';
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {ProjectFeedback} from "../../../../fyp_everything_modules/model/m_project/ProjectFeedback";

@Injectable()
export class ProjectFeedbackProvider {

  public totalFeedback;
  public avgRating;
  public myFeedback = new ProjectFeedback();
  public feedbackList = [];

  constructor() {

  }

  fetchAllRecords(projectID) {

    let mySettings = new Settings('subscribed-project/feedback');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([projectID], (response) => {

      this.avgRating = 0;
      this.myFeedback = new ProjectFeedback();
      this.feedbackList = [];
      this.totalFeedback = 0;

      this.loadOverallInfo(response.overallInfo[0]);

      if(response.userFeedback[0] != null)
      {
        this.loadMyFeedback(response.userFeedback[0]);
      }

      if(response.otherUserFeedback != null)
      {
        this.loadFeedbackList(response.otherUserFeedback);
      }

    });
  }

  private loadOverallInfo(data) {
    this.avgRating = data.avgRating;
    this.totalFeedback = data.noReview;
  }

  private loadMyFeedback(data) {

    this.myFeedback.mapData(data);
  }

  private loadFeedbackList(data) {
    this.feedbackList = [];

    for (let i = 0; i < data.length; i++) {
      let temp = new ProjectFeedback();
      temp.mapData(data[i]);

      this.feedbackList.push(temp);
    }
  }

}
