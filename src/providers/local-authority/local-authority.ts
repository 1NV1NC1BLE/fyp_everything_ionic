import {Injectable} from '@angular/core';

import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {LocalAuthority} from "../../../../fyp_everything_modules/model/LocalAuthority";


@Injectable()
export class LocalAuthorityProvider {

  public list = [];

  constructor() {
  }

  fetchAllRecords() {

    let mySettings = new Settings('local-authority/index');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);

    myAjax.getMethod((response) => {

      this.list = [];
      for (let i = 0; i < response.length; i++) {

        let temp = new LocalAuthority();
        temp.mapData(response[i]);
        this.list.push(temp);

      }
    });
  }
}
