
import { Injectable } from '@angular/core';
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {UserComplaintTicket} from "../../../../fyp_everything_modules/model/complaint/UserComplaintTicket";

@Injectable()
export class UserComplaintTicketProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords(){
    let me = this;

    let mySettings = new Settings('complaint-ticket/public-user/my-ticket');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getMethod(function (response) {

      me.list = [];
      for(let i = 0; i<response.length;i++) {
        let temp = new UserComplaintTicket();
        temp.mapData(response[i]);
        me.list.push(temp);
      }

    });
  }

  findItem(id:number):any{


    return Object.keys(this.list)[id];


  }
}
