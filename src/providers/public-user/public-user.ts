import {Injectable} from '@angular/core';
import {PublicUser} from "../../../../fyp_everything_modules/model/users/PublicUser";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";

@Injectable()
export class PublicUserAccountInfoProvider {

  public myUser = new PublicUser();

  constructor() {

  }

  getProfileData() {

    let mySetting = new Settings('profile/public-user');
    mySetting.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySetting);
    myAjax.getMethod((response) => {
      this.myUser.mapData(response[0]);

    }, () => {

    });
  }

}
