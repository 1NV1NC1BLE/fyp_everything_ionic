import {Injectable} from '@angular/core';
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {ProjectProgress} from "../../../../fyp_everything_modules/model/m_project/ProjectProgress";

@Injectable()
export class ProjectProgressProvider {

  public list = [];

  constructor() {

  }

  fetchAllRecords(projectID) {

    let me = this;

    let mySettings = new Settings('subscribed-project/progress');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([projectID], function (response) {

      me.list = [];
      for (let i = 0; i < response.length; i++) {
        let temp = new ProjectProgress();
        temp.mapData(response[i]);
        me.list.push(temp);
      }

    });
  }

}
