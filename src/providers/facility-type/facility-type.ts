import {Injectable} from '@angular/core';
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {FacilityType} from "../../../../fyp_everything_modules/model/complaint/FacilityType";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";

@Injectable()
export class FacilityTypeProvider {

  public list = [];

  constructor() {
  }

  fetchAllRecords() {

    let me = this;

    let mySettings = new Settings('facility-type/index');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getMethod(function (response) {

      me.list = [];
      for (let i = 0; i < response.length; i++) {
        let temp = new FacilityType();
        temp.mapData(response[i]);
        me.list.push(temp);
      }
    });
  }
}
