
import { Injectable } from '@angular/core';
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {MaintenanceProject} from "../../../../fyp_everything_modules/model/m_project/MaintenanceProject";

/*
  Generated class for the AllProjectProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class OtherProjectProvider {

  public list = [];

  constructor() {

  }

  fetchLocalAuthorityProjectList(id){

    let mySettings = new Settings('other-project');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getWithParaMethod([id],(response) => {

      this.list = [];

      for (let i = 0; i < response.length; i++) {

        let temp = new MaintenanceProject();
        temp.mapData(response[i]);
        this.list.push(temp);

      }
    }, (response)=>{
      this.list = [];
    });
  }


}
