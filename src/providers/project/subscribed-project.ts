import {Injectable} from '@angular/core';
import {AjaxHandler} from "../../../../fyp_everything_modules/http/AjaxHandler";
import {Settings} from "../../../../fyp_everything_modules/http/Settings";
import {MaintenanceProject} from "../../../../fyp_everything_modules/model/m_project/MaintenanceProject";


@Injectable()
export class SubscribedMaintenanceProjectProvider {

  public list = [];

  constructor() {

  }

  fetchUserSubscribedProjectList() {

    let mySettings = new Settings('subscribed-project');

    mySettings.token = window.localStorage.getItem('token');

    let myAjax = new AjaxHandler(mySettings);
    myAjax.getMethod((response) => {

      this.list = [];

      for (let i = 0; i < response.length; i++) {

        let temp = new MaintenanceProject();
        temp.mapData(response[i]);
        this.list.push(temp);

      }
    });
  }
}
