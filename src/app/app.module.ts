import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatStepperModule } from '@angular/material/stepper';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule, MatAutocompleteModule } from "@angular/material";
import { Geolocation } from '@ionic-native/geolocation';
import { Ionic2RatingModule } from "ionic2-rating";

import { MyApp } from './app.component';
import { DashBoardPage } from '../pages/dashboard/dashboard';
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/register/register";
import { SubmitTicketPage } from "../pages/submit-ticket/submit-ticket";
import { ViewTicketPage } from "../pages/view-ticket/view-ticket";
import { TicketPage } from "../pages/ticket/ticket";
import { MessagePage } from "../pages/message/message";
import { SubscribedProjectPage } from "../pages/subscribed-project/subscribed-project";
import { OtherProjectPage } from "../pages/other-project/other-project";
import { ProfilePage } from "../pages/profile/profile";
import { PublicUserAccountInfoProvider } from '../providers/public-user/public-user';
import { LocalAuthorityProvider } from '../providers/local-authority/local-authority';
import { FacilityTypeProvider } from '../providers/facility-type/facility-type';
import { UserComplaintTicketProvider } from '../providers/complaint-ticket/complaint-ticket';
import { MessagesProvider } from '../providers/messages/messages';
import { SubscribedMaintenanceProjectProvider } from '../providers/project/subscribed-project';
import { ProjectProgressProvider } from '../providers/project-progress/project-progress';
import { ProjectFeedbackProvider } from '../providers/project-feedback/project-feedback';
import { ProjectDetailsPage } from "../pages/project-details/project-details";
import { ProjectProgressPage } from "../pages/project-progress/project-progress";
import { ProjectFeedbackPage } from "../pages/project-feedback/project-feedback";
import { FeedbackModalPage } from "../pages/feedback-modal/feedback-modal";
import { OtherProjectSelectLaPage } from "../pages/other-project-select-la/other-project-select-la";
import { OtherProjectProvider } from '../providers/other-project/other-project';
import { ChangePasswordPage } from "../pages/change-password/change-password";
import { ForgotPassPage } from "../pages/forgot-pass/forgot-pass";
import { AndroidPermissions } from "@ionic-native/android-permissions";
import { CommonModule } from '@angular/common';
import { LoginPageModule } from '../pages/login/login.module';
import { RegisterPageModule } from '../pages/register/register.module';
import { ForgotPassPageModule } from '../pages/forgot-pass/forgot-pass.module';
import { SubmitTicketPageModule } from '../pages/submit-ticket/submit-ticket.module';
import { ViewTicketPageModule } from '../pages/view-ticket/view-ticket.module';
import { TicketPageModule } from '../pages/ticket/ticket.module';
import { MessagePageModule } from '../pages/message/message.module';
import { SubscribedProjectPageModule } from '../pages/subscribed-project/subscribed-project.module';
import { ProjectDetailsPageModule } from '../pages/project-details/project-details.module';
import { ProjectProgressPageModule } from '../pages/project-progress/project-progress.module';
import { ProjectFeedbackPageModule } from '../pages/project-feedback/project-feedback.module';
import { FeedbackModalPageModule } from '../pages/feedback-modal/feedback-modal.module';
import { OtherProjectPageModule } from '../pages/other-project/other-project.module';
import { OtherProjectSelectLaPageModule } from '../pages/other-project-select-la/other-project-select-la.module';
import { ChangePasswordPageModule } from '../pages/change-password/change-password.module';
import { ProfilePageModule } from '../pages/profile/profile.module';

@NgModule({
  declarations: [
    MyApp,
    DashBoardPage,

    // LoginPage,
    // RegisterPage,
    // ForgotPassPage,
    // SubmitTicketPage,
    // ViewTicketPage,
    // TicketPage,
    // MessagePage,
    // SubscribedProjectPage,
    // ProjectDetailsPage,
    // ProjectProgressPage,
    // ProjectFeedbackPage,
    // FeedbackModalPage,
    // OtherProjectPage,
    // OtherProjectSelectLaPage,
    // ChangePasswordPage,
    // ProfilePage,

  ],
  imports: [
    IonicModule.forRoot(MyApp),
    CommonModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatInputModule,
    MatStepperModule,
    MatFormFieldModule,
    MatIconModule,
    MatAutocompleteModule,
    Ionic2RatingModule,



    LoginPageModule,
    RegisterPageModule,
    ForgotPassPageModule,
    SubmitTicketPageModule,
    ViewTicketPageModule,
    TicketPageModule,
    MessagePageModule,
    SubscribedProjectPageModule,
    ProjectDetailsPageModule,
    ProjectProgressPageModule,
    ProjectFeedbackPageModule,
    FeedbackModalPageModule,
    OtherProjectPageModule,
    OtherProjectSelectLaPageModule,
    ChangePasswordPageModule,
    ProfilePageModule,

  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashBoardPage,
    LoginPage,
    RegisterPage,
    ForgotPassPage,
    SubmitTicketPage,
    ViewTicketPage,
    TicketPage,
    MessagePage,
    SubscribedProjectPage,
    ProjectDetailsPage,
    ProjectProgressPage,
    ProjectFeedbackPage,
    FeedbackModalPage,
    OtherProjectPage,
    OtherProjectSelectLaPage,
    ProfilePage,
    ChangePasswordPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    AndroidPermissions,
    { provide: ErrorHandler, useClass: IonicErrorHandler },

    PublicUserAccountInfoProvider,
    LocalAuthorityProvider,
    FacilityTypeProvider,
    UserComplaintTicketProvider,
    MessagesProvider,
    SubscribedMaintenanceProjectProvider,
    ProjectProgressProvider,
    ProjectFeedbackProvider,
    OtherProjectProvider,
  ]
})
export class AppModule { }
