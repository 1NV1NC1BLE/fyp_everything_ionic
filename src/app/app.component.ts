import {Component, ViewChild} from '@angular/core';
import {Nav, Platform, LoadingController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';

import {DashBoardPage} from '../pages/dashboard/dashboard';
import {LoginPage} from "../pages/login/login";
import {SubmitTicketPage} from "../pages/submit-ticket/submit-ticket";
import {ViewTicketPage} from "../pages/view-ticket/view-ticket";
import {SubscribedProjectPage} from "../pages/subscribed-project/subscribed-project";
import {ProfilePage} from "../pages/profile/profile";

import {Settings} from "../../../fyp_everything_modules/http/Settings";
import {AjaxHandler} from "../../../fyp_everything_modules/http/AjaxHandler";
import {PublicUserAccountInfoProvider} from "../providers/public-user/public-user";
import {OtherProjectSelectLaPage} from "../pages/other-project-select-la/other-project-select-la";

@Component({
  templateUrl: 'app.html',

})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              public loadingCtrl: LoadingController,
              public pbUser: PublicUserAccountInfoProvider) {


   let autoLoginPromise =  ()=> {
     return new Promise((resolve)=>{

       let mySetting = new Settings('autoLogin');
       mySetting.token = window.localStorage.getItem('token');

       let myAjax = new AjaxHandler(mySetting);
       myAjax.getMethod((response)=>{
         this.rootPage = DashBoardPage;
         resolve();
       }, ()=>{
         this.rootPage = LoginPage;
         resolve();
       });

     });
   };

    platform.ready().then(()=>{

      if (window.localStorage.getItem('isRemember') == 'true') {
        autoLoginPromise();
      }
      else {
        this.rootPage = LoginPage;
      }

    }).then(()=>{
      if(!document.URL.startsWith('http'))
      {
        statusBar.styleLightContent();
        statusBar.backgroundColorByHexString('#003699');
        splashScreen.hide();
      }
    });

  }

  openHomePage() {
      this.nav.setRoot(DashBoardPage);
  }

  openSubmitTicketPage() {
    this.nav.push(SubmitTicketPage);
  }

  openViewTicketPage() {
    this.nav.setRoot(ViewTicketPage);
  }

  openSubscribedProjectPage() {
    this.nav.setRoot(SubscribedProjectPage);
  }

  openOtherProjectPage() {
    this.nav.push(OtherProjectSelectLaPage);
  }

  openProfilePage() {
    this.nav.setRoot(ProfilePage);
  }

  onLogout() {

    let loading = this.loadingCtrl.create({content: "Logging Out!"});

    let logoutPromise = new Promise((resolve) => {

      let mySettings = new Settings("logout");
      mySettings.token = window.localStorage.getItem('token');

      let myAjax = new AjaxHandler(mySettings);

      myAjax.postMethod(() => {
        resolve();

      }, () => {
        resolve();
      });
    });

    loading.present().then(() => {

      logoutPromise.then(() => {

        window.localStorage.removeItem('token');
        this.nav.setRoot(LoginPage);

      }).then(() => {

        loading.dismiss();

      });
    });


  }
}

